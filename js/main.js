const cartoDbUrl = 'https://cartodb-basemaps-{s}.global.ssl.fastly.net'
const cartoDbAttribution = `
  ©<a href=http://www.openstreetmap.org/copyright>OpenStreetMap</a>,
  ©<a href=https://carto.com/attribution>CARTO</a>
`

function initMap (config) {
  map = L.map('map')
  map.createPane('labels')
  map.getPane('labels').classList.add('leaflet-pane-labels')
  // TODO: deal with invalid hash.
  if (!window.location.hash) { map.setView([48.73986, 2.30713], 6) }
  const hash = new L.Hash(map)

  L.tileLayer(`${cartoDbUrl}/light_nolabels/{z}/{x}/{y}{r}.png`, {
    maxZoom: 18, attribution: cartoDbAttribution
  }).addTo(map)

  new FlightAreas(map, config)

  L.tileLayer(`${cartoDbUrl}/light_only_labels/{z}/{x}/{y}{r}.png`, {
    pane: 'labels', maxZoom: 18, attribution: cartoDbAttribution
  }).addTo(map)

  drones = L.featureGroup().addTo(map)
}

class ServerError extends Error {}

function request (url, options) {
  return fetch(url, options)
          .then(response => [response, response.json()])
          .then(([response, data]) => {
            if (response.ok) {
              return data
            } else {
              const e = new ServerError(`${url} ${response.status} ${data}`)
              e.status = response.status
              e.url = url
              e.data = data
              throw e
            }
          })
          .catch(error => {
            if (error instanceof ServerError) throw error
            const e = new Error(`${error.message} ${url}`)
            e.url = url
            throw e
          })
}

function handleError (error) {
  console.error(error)
  const errorURL = new window.URL(error.url)
  const userMessage = `
    Le domaine ${errorURL.host} semble être inaccessible.
    Nous en avons été informés, veuillez réessayer plus tard.
  `
  let toastElement = document.querySelector('.navbar .toast')
  if (!toastElement) {
    const template = document.querySelector('#template-error')
    const navbar = document.querySelector('.navbar')
    const clone = document.importNode(template.content, true)
    navbar.appendChild(clone)
    toastElement = document.querySelector('.navbar .toast')
  }
  toastElement.classList.remove('hide')
  if (toastElement.innerHTML.indexOf(userMessage) === -1) {
    const p = document.createElement('p')
    p.textContent = userMessage
    p.classList.add('text-center')
    toastElement.appendChild(p)
  }
  toastElement.querySelector('.btn-clear').addEventListener('click', event => {
    event.preventDefault()
    toastElement.classList.add('hide')
  })
}

const icons = L.Icon.extend({
  options: {
    shadowUrl: 'vendor/leaflet/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  }
})

function createIcon (color) {
  return new L.icons({
    iconUrl: `img/leaflet-color-markers/marker-icon-${color}.png`,
    iconRetinaUrl: `img/leaflet-color-markers/marker-icon-2x-${color}.png`
  })
}

const blueIcon = createIcon('blue')
const redIcon = createIcon('red')
const greenIcon = createIcon('green')
const greyIcon = createIcon('grey')

function selectIcon (position) {
  if (position.allowed === undefined) {
    return blueIcon
  } else {
    if (position.allowed === null) {
      return greyIcon
    } else if (!position.allowed) {
      return redIcon
    } else {
      return greenIcon
    }
  }
}

function toQueryParams (data) {
  return Object.keys(data).map(k => `${k}=${data[k]}`).join('&')
}

function getBBox (map) {
  const bounds = map.getBounds()
  return {
    west: bounds.getWest(),
    south: bounds.getSouth(),
    east: bounds.getEast(),
    north: bounds.getNorth()
  }
}
