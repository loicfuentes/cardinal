const FlightAreas = L.TileLayer.extend({

  options: {
    keepBuffer: 40  // Never reload a tile on a same zoom level for now.
                    // TODO: deal with tile pruning.
  },

  initialize (map, config) {
    this.config = config
    L.TileLayer.prototype.initialize.call(this, config.tilesURL)
    this.addTo(map)
  },

  onLayerClick (event) {
    const {lat, lng} = event.latlng
    const data = {
      lat: lat,
      lon: lng,
      alt: 0
    }
    request(`${this.config.checkURL}?${toQueryParams(data)}`)
      .then(response => {
        let content
        if (response.allowed === true) {
          content = 'Vol autorisé sous conditions'
        } else if (response.ceiling > 0) {
          content = `Hauteur limitée : ${response.ceiling} mètres`
        } else if (response.allowed === false) {
          content = `
            Vol interdit, zone(s) concernée(s) :
            <ul>${response.areas.map(area => `
                <li>${area.label}</li>
            `).join('')}</ul>`
        } else {
          content = `
            Se renseigner auprès des autorités compétentes
            sur la zone.
            <dl>${response.areas.map(area => `
                <dt>${area.label}</dt>
                <dd>${area.description}</dd>
            `).join('')}</dl>
          `
        }
        event.target.bindPopup(content).openPopup(event.latlng)
      })
      .catch(handleError)
  },

  initVectorLayer () {
    this.vectorlayer = new L.GeoJSON(null, {
      pointToLayer: (feature, latlng) => L.circleMarker(latlng, {radius: 5}),
      onEachFeature: (feature, layer) => layer.on('click', this.onLayerClick, this)
    }).addTo(this._map)
  },

  removeVectorLayer () {
    this._map.removeLayer(this.vectorlayer)
  },

  onZoomStart () {
    this.abortLoading()
    this.vectorlayer.clearLayers()
  },

  onAdd (map) {
    this._map = map
    this.options.version = Date.now()
    this._processing = []
    this._tileCache = {}
    this.initVectorLayer()
    map.on('zoomstart', this.onZoomStart, this)
    L.TileLayer.prototype.onAdd.call(this, map)
  },

  onRemove (map) {
    this.abortLoading()
    map.off('zoomstart', this.onZoomStart, this)
    this.removeVectorLayer()
    L.TileLayer.prototype.onRemove.call(this, map)
  },

  abortLoading () {
    this._processing.forEach(processing => {
      processing.aborted = true // Flag to distinguish from NetworkError.
      processing.abort()
    })
  },

  _addTile (tilePoint, container) {
    L.TileLayer.prototype._addTile.call(this, tilePoint, container)
    const processTile = data => {
      try {
        this.addData(JSON.parse(data), tilePoint)
      } catch (err) {  // Sometimes Mapnik gives invalid geojson
        console.error(err)
        console.log(data)
      }
    }
    const url = this.getTileUrl(tilePoint)
    if (this._tileCache[url]) {
      processTile(this._tileCache[url])
    } else {
      const req = this.request(url, (status, data) => {
        const index = this._processing.indexOf(req)
        if (this._processing[index].aborted) {
          // Do not do anything, normal abort from the system.
        } else if (status === 0) {
          const e = new Error(`NetworkError when attempting to fetch resource. ${url}`)
          e.url = url
          return handleError(e)
        } else if (status !== 200) {
          const e = new ServerError(`${url} ${status} ${data}`)
          e.status = status
          e.url = url
          e.data = data
          return handleError(e)
        } else {
          this._tileCache[url] = data
          processTile(data)
          if (index !== -1) this._processing.splice(index, 1)
        }
      })
      this._processing.push(req)
    }
  },

  addData (data, tilePoint) {
    let layer
    let feature
    const tileSize = this.options.tileSize
    const nwPoint = tilePoint.multiplyBy(tileSize)
    const swPoint = nwPoint.add([0, tileSize])
    const nePoint = nwPoint.add([tileSize, 0])
    const sw = this._map.unproject(swPoint)
    const ne = this._map.unproject(nePoint)
    const tileBounds = L.latLngBounds(sw, ne)

    data.features.forEach(feature => {
      try {
        layer = L.GeoJSON.geometryToLayer(feature)
      } catch (err) {
        console.log(err)
        return
      }
      layer.feature = L.GeoJSON.asFeature(feature)
      const colors = {
        100: 'rgb(255,234,0)',
        60: 'rgb(255,191,0)',
        50: 'rgb(255,160,0)',
        30: 'rgb(255,106,0)',
        false: 'green',
        null: 'grey'
      }
      let color = 'rgb(200,0,0)'
      if (feature.properties.floor in colors) {
        color = colors[feature.properties.floor]
      } else if (feature.properties.forbidden in colors) {
        color = colors[feature.properties.forbidden]
      }
      layer.setStyle({
        stroke: false,
        fill: true,
        fillOpacity: 0.6,
        fillColor: color
      })
      this.vectorlayer.options.onEachFeature(feature, layer)
      this.vectorlayer.addLayer(layer)
    })
  },

  redraw (force) {
    if (force) this._tileCache = {}
    if (this.vectorlayer) this.vectorlayer.clearLayers()
    L.TileLayer.prototype.redraw.call(this)
  },

  request (uri, callback) {
    const xhr = new window.XMLHttpRequest()
    xhr.open('GET', uri, true)
    xhr.onreadystatechange = _ => {
      if (xhr.readyState === 4) {
        callback.call(this, xhr.status, xhr.responseText)
      }
    }
    xhr.send()
    return xhr
  }

})
